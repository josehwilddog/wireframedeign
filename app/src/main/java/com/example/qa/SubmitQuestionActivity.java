package com.example.qa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import models.Question;

public class SubmitQuestionActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private static final DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_question);


        Button submitBtn = (Button) findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText titleET= (EditText) findViewById(R.id.titleET);
                EditText contentET = (EditText) findViewById(R.id.contentET);
                String title = titleET.getText().toString();
                String content = contentET.getText().toString();

                Calendar cal = Calendar.getInstance();


                Question question = new Question(sdf.format(cal.getTime()),title, content,
                        FirebaseAuth.getInstance().getCurrentUser().getUid());

                FirebaseDatabase.getInstance().getReference().child("questions").push()
                        .setValue(question, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference reference) {
                                if (databaseError != null) {
                                    Toast.makeText(SubmitQuestionActivity.this,"Unable to submit question",
                                            Toast.LENGTH_SHORT).show();
                                } else {
                                    startActivity(new Intent(SubmitQuestionActivity.this, MainActivity.class));
                                    Toast.makeText(SubmitQuestionActivity.this,"Question submitted",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });
    }
}
