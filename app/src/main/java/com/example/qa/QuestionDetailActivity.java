package com.example.qa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import models.Answer;
import models.Question;

public class QuestionDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_detail);

        Question question = (Question) getIntent().getSerializableExtra("question");
        loadQuestion(question);

        ListView listView = (ListView) findViewById(R.id.answersLV);

        String questionKey = getIntent().getStringExtra("question_key");

        Query query = FirebaseDatabase.getInstance().getReference()
                .child("answers").child(questionKey);

        //get instance of database
        DatabaseReference answersDatabase = FirebaseDatabase.getInstance().getReference()
                .child("answers").child(questionKey);

        FirebaseListOptions<Answer> options = new FirebaseListOptions.Builder<Answer>()
                .setQuery(query,Answer.class)
                .setLayout(R.layout.constraint_layout)
                .setLifecycleOwner(this)
                .build();

        FirebaseListAdapter answerAdapter = new FirebaseListAdapter<Answer>(options) {
            @Override
            protected void populateView(View view, Answer answer, final int position) {
                ((TextView)view.findViewById(R.id.content)).setText(answer.getText());
                ((TextView)view.findViewById(R.id.name)).setText(answer.getUuid());

            }
        };

        listView.setAdapter(answerAdapter);
    }
    private void loadQuestion(Question question){
        TextView questionTextView = (TextView) findViewById(R.id.questionTV);
        questionTextView.setText(question.getTitle());

        TextView descriptionTextView = (TextView) findViewById(R.id.descriptionTV);
        descriptionTextView.setText(question.getContent());
    }

    public void submitAnswer(View view){
        Intent i = new Intent(this, SubmitAnswerActivity.class);
        i.putExtra("question_key", getIntent().getStringExtra("question_key"));
        startActivity(i);
    }
}
