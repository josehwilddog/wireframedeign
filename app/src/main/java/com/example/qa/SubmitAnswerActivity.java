package com.example.qa;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import models.Answer;

public class SubmitAnswerActivity extends AppCompatActivity {
    private static final DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_answer);
    }

    public void submitAnswer(View view){
        EditText answerET= (EditText) findViewById(R.id.answerET);
        String text = answerET.getText().toString();
        Calendar cal = Calendar.getInstance();

        Answer answer = new Answer(sdf.format(cal.getTime()),text,
                FirebaseAuth.getInstance().getCurrentUser().getUid());

        String questionKey = getIntent().getStringExtra("question_key");


        FirebaseDatabase.getInstance().getReference().child("answers").child(questionKey).push()
                .setValue(answer, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference reference) {
                        if (databaseError != null) {
                            Toast.makeText(SubmitAnswerActivity.this,"Unable to submit question",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            finish();
                            Toast.makeText(SubmitAnswerActivity.this,"Question submitted",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
