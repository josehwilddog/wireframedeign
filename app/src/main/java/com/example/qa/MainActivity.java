package com.example.qa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.io.Serializable;

import models.Answer;
import models.Question;

public class MainActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private   FirebaseListAdapter<Question> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user == null){
            Log.v("MainActivity", "not signed in");
            startActivity(new Intent(this, LoginActivity.class));
        } else {
            setContentView(R.layout.activity_main);

            Button askBtn = (Button) findViewById(R.id.askBtn);
            askBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this,SubmitQuestionActivity.class));
                }
            });

            ListView listView = (ListView) findViewById(R.id.questionsLV);

            Query query = FirebaseDatabase.getInstance().getReference().child("questions");

            //get instance of database
            mDatabase = FirebaseDatabase.getInstance().getReference().child("questions");

            FirebaseListOptions<Question> options = new FirebaseListOptions.Builder<Question>()
                    .setQuery(query, Question.class)
                    .setLayout(R.layout.list_item)
                    .setLifecycleOwner(this)
                    .build();

                mAdapter = new FirebaseListAdapter<Question>(options) {
                @Override
                protected void populateView(View view, Question question, final int position) {
                    ((TextView)view.findViewById(R.id.name)).setText(question.getUuid());
                    ((TextView)view.findViewById(R.id.time)).setText(question.getmDate());
                    //((TextView)view.findViewById(R.id.tv_title)).setText(question.getTitle());
                    ((TextView)view.findViewById(R.id.message_body)).setText(question.getContent());

                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(MainActivity.this, QuestionDetailActivity.class);
                            i.putExtra("question", (Serializable) mAdapter.getItem(position));
                            i.putExtra("question_key", mAdapter.getRef(position).getKey());
                            startActivity(i);
                        }
                    });



                }
            };

            listView.setAdapter(mAdapter);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public void signOutUser(){
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(this, LoginActivity.class));
    }

    public void signOutUser(View view) {
    }
}
