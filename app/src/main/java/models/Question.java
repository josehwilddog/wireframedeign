package models;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by diyahm-pl on 8/3/16.
 */
public class Question implements Serializable {
    private static final long serialVersionUID = -56543230L;
    private String title;
    private String mDate;
    private String content;
    private String uuid;

    public Question(){

    }

    public Question(String mDate,String title, String content, String uuid){
        this.title = title;
        this.content = content;
        this.uuid = uuid;
        this.mDate = mDate;
    }
    public String getTitle(){
        return title;
    }

    public String getContent(){
        return content;
    }

    public String getUuid(){
        return uuid;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setContent(String content){
        this.content = content;
    }

    public void setUuid(String uuid){
        this.uuid = uuid;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }
}
