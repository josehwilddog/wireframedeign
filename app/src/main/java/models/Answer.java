package models;

import java.io.Serializable;

/**
 * Created by diyahm-pl on 8/11/16.
 */
public class Answer implements Serializable {
    private static final long serialVersionUID = -56543888L;
    private String text;
    private String uuid;
    private String mDate;

    public Answer(){

    }

    public Answer(String mDate,String text, String uuid){
        this.mDate = mDate;
        this.text = text;
        this.uuid = uuid;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }

    public String getText(){
        return text;
    }

    public String getUuid(){
        return uuid;
    }

    public void setText(String text){
        this.text = text;
    }

    public void setUuid(String uuid){
        this.uuid = uuid;
    }

}
